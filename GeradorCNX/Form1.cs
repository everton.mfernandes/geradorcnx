﻿using GeradorCNX.classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GeradorCNX
{
    public partial class frmGerarCNX : Form
    {
        public frmGerarCNX()
        {
            InitializeComponent();
        }

        private void btnGerarCNX_Click(object sender, EventArgs e)
        {
            {
                XmlWriter writer = null;

                try
                {
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.IndentChars = "\t";
                    settings.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    writer = XmlWriter.Create("c:\\temp\\teste.cnx", settings);

                    writer.WriteStartElement("mensagemSIB");
                    escreveCabecalho(writer);
                    writer.WriteStartElement("mensagem");
                    writer.WriteStartElement("ansParaOperadora");
                    writer.WriteStartElement("conferencia");
                    P12117MNTEntities context = new P12117MNTEntities();
                    List<B3KT10> list = context.Set<B3KT10>().ToList();
                    progress.Step = 0;
                    progress.Maximum = list.Count;
                    foreach ( var beneficiario in list){
                        escreveCorpo(writer,beneficiario);
                        progress.Increment(1);
                        progress.PerformStep();
                    };
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.Flush();
                    writer.Close();
                    MessageBox.Show("CNX gerado");
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                }
            }
        }

        private static void escreveCabecalho(XmlWriter writer)
        {
            writer.WriteStartElement("cabecalho");
            writer.WriteStartElement("identificacaoTransacao");
            writer.WriteStartElement("tipoTransacao");
            writer.WriteString("CONFERENCIA CADASTRAL");
            writer.WriteEndElement();
            writer.WriteStartElement("sequencialTransacao");
            writer.WriteString("1");
            writer.WriteEndElement();
            writer.WriteStartElement("dataHoraRegistroTransacao");
            writer.WriteString("2011-08-12T12:51:21.262");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("origem");
            writer.WriteStartElement("cnpj");
            writer.WriteString("03589068000146");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("destino");
            writer.WriteStartElement("registroANS");
            writer.WriteString("304883");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("versaoPadrao");
            writer.WriteString("1.1");
            writer.WriteEndElement();
            writer.WriteStartElement("identificacaoSoftwareGerador");
            writer.WriteStartElement("nomeAplicativo");
            writer.WriteString("SIB-XML-CONFERENCIA");
            writer.WriteEndElement();
            writer.WriteStartElement("versaoAplicativo");
            writer.WriteString("ANS");
            writer.WriteEndElement();
            writer.WriteStartElement("fabricanteAplicativo");
            writer.WriteString("1.1.1");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        private static void escreveCorpo(XmlWriter writer, B3KT10 beneficiario)
        {
            var dataNascimento = beneficiario.B3K_DATNAS.ToString(); 
            writer.WriteStartElement("beneficiario");
            writer.WriteAttributeString("cco", beneficiario.B3K_CODCCO);
            writer.WriteAttributeString("situacao", "ATIVO");
            writer.WriteAttributeString("dataAtualizacao", "2010-04-08");
            writer.WriteStartElement("identificacao");
            writer.WriteStartElement("cpf");
            writer.WriteString(beneficiario.B3K_CPF.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("nome");
            writer.WriteString(beneficiario.B3K_NOMBEN.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("sexo");
            writer.WriteString(beneficiario.B3K_SEXO.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("dataNascimento");
            writer.WriteString(dataNascimento.Substring(0,4) + "-" + dataNascimento.Substring(4, 2) + "-" + dataNascimento.Substring(6, 2));
            writer.WriteEndElement();
            writer.WriteStartElement("nomeMae");
            writer.WriteString(beneficiario.B3K_NOMMAE.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("endereco");
            writer.WriteStartElement("logradouro");
            writer.WriteString(beneficiario.B3K_ENDERE.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("numero");
            writer.WriteString(beneficiario.B3K_NR_END.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("bairro");
            writer.WriteString(beneficiario.B3K_BAIRRO.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("codigoMunicipio");
            writer.WriteString(beneficiario.B3K_CODMUN.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("cep");
            writer.WriteString(beneficiario.B3K_CEPUSR.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("resideExterior");
            writer.WriteString(beneficiario.B3K_RESEXT.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("vinculo");
            writer.WriteStartElement("codigoBeneficiario");
            writer.WriteString(beneficiario.B3K_MATRIC.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("relacaoDependencia");
            writer.WriteString(beneficiario.B3K_TIPDEP.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("dataContratacao");
            writer.WriteString("2010-03-31");
            writer.WriteEndElement();
            writer.WriteStartElement("numeroPlanoANS");
            writer.WriteString(beneficiario.B3K_SUSEP.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("coberturaParcialTemporaria");
            writer.WriteString(beneficiario.B3K_COBPAR.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("itensExcluidosCobertura");
            writer.WriteString(beneficiario.B3K_ITEEXC.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteStartElement("cnpjEmpresaContratante");
            writer.WriteString(beneficiario.B3K_CNPJCO.ToString().Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
                    
        }

    }
}
