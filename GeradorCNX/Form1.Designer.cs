﻿namespace GeradorCNX
{
    partial class frmGerarCNX
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnGerarCNX = new System.Windows.Forms.Button();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // btnGerarCNX
            // 
            this.btnGerarCNX.Location = new System.Drawing.Point(94, 41);
            this.btnGerarCNX.Name = "btnGerarCNX";
            this.btnGerarCNX.Size = new System.Drawing.Size(124, 23);
            this.btnGerarCNX.TabIndex = 0;
            this.btnGerarCNX.Text = "Gerar CNX";
            this.btnGerarCNX.UseVisualStyleBackColor = true;
            this.btnGerarCNX.Click += new System.EventHandler(this.btnGerarCNX_Click);
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(40, 96);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(230, 23);
            this.progress.Step = 0;
            this.progress.TabIndex = 1;
            this.progress.UseWaitCursor = true;
            // 
            // frmGerarCNX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 149);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.btnGerarCNX);
            this.Name = "frmGerarCNX";
            this.Text = "Gerar CNX";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnGerarCNX;
        private System.Windows.Forms.ProgressBar progress;
    }
}

